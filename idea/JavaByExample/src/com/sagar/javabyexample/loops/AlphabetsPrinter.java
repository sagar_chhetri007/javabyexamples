package com.sagar.javabyexample.loops;

public class AlphabetsPrinter {
    public static void main(String [] args){
        System.out.println(" Prints the alphabets from A to Z");
        for (int ch = 'A';ch <= 'Z';ch++){
            System.out.println((char)ch);
        }
    }
}
