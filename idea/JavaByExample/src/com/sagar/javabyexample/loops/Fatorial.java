package com.sagar.javabyexample.loops;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Fatorial {
    public static void main(String[] args){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            System.out.println("Enter the positive number ");
            String input = br.readLine();
            System.out.println("the enter number is : " +input);
            int num = Integer.parseInt(input);
            long factorial = 1 ;
            for (int i=num ; i > 0; i-- ){
                 factorial = factorial * i;
            }
            System.out.println("The factorial of number "+num+" is "+factorial);

        }catch (Exception ex){
            ex.getMessage();
        }
    }
}
