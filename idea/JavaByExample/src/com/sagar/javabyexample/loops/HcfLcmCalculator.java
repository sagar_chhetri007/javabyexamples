package com.sagar.javabyexample.loops;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HcfLcmCalculator {
    public static void main(String[] args){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String input;
        try {
            System.out.println("Enter the first number: ");
            input = br.readLine();
            int num1 = Integer.parseInt(input);
            System.out.println("Enter the Second number: ");
            input = br.readLine();
            int num2 = Integer.parseInt(input);
            System.out.println("the enter number is : " +num1+" and "+num2);
            System.out.println("the Hcf is : "+checkHcf(num1,num2));
            System.out.println("the Lcm is : "+checkLcm(num1,num2));

        }catch (Exception ex){
            ex.getMessage();
        }
    }
    private static int checkHcf(int num1,int num2){
        int min = Math.min(num1,num2);
        int hcf = 0;
        for(int i = min;i>=1;i--){
            if (num1 % i == 0 && num2 % i == 0){
                hcf = i;
                break;
            }
        }
        return hcf;
    }
    private static int checkLcm(int num1,int num2){
        int hcf = checkHcf(num1,num2);

        return (num1 * num2) /hcf;
    }
}
