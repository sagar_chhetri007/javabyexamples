package com.sagar.javabyexample.loops;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PrimeNumberPrinters {
    public static void main(String[] args){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            System.out.println("Enter the positive number ");
            String input = br.readLine();
            System.out.println("the enter number is : " +input);
            int num = Integer.parseInt(input);
            Integer[] primenumbers = {2,3};
            int count = 0;
            System.out.println("the prime numbers are: \n " + 2 +" \n"+3);
            for (int i = 4;i<= num;i++){
              /*  if(i%2 != 0){
                    int maxcount = (int)Math.ceil(Math.sqrt(i));
                    for (int j = 3; j < maxcount ; j = j + 2){
                        if (i%3 != 0){
                            System.out.println(i);
                           // primenumbers[count] = i;
                            //count ++;
                        }
                    }
                }*/
              if (checkPrime(i)){
                  System.out.println(i);
              }
            }


        }catch (Exception ex){
            ex.getMessage();
        }
    }
    private static boolean checkPrime(int i){
        boolean isPrime = false;
        if(i%2 != 0){
            int maxcount = (int)Math.ceil(Math.sqrt(i));
            for (int j = 3; j < maxcount ; j = j + 2){
                if (i%3 != 0){
                    isPrime = true;
                }
            }
        }
      return isPrime;
    }
}
