package com.sagar.javabyexample.Arrays;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class FindLargest {
    public static void main(String[] args){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            System.out.println("Enter the positive number ");
            String input = br.readLine();
            System.out.println("the enter number is : " +input);
            int num = Integer.parseInt(input);
            int[] numbers = new int[num];

            for (int i=0 ; i <num; i++){
                numbers[i] = (int)(Math.random() * 123);
            }
            for (int i=0 ; i <num; i++){
                System.out.print(numbers[i]+" ");
            }
            Arrays.sort(numbers);
            System.out.println(" Largest : "+numbers[num - 1]);


        }catch (Exception ex){
            ex.getMessage();
        }
    }
}
