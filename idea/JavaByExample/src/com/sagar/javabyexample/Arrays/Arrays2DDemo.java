package com.sagar.javabyexample.Arrays;

public class Arrays2DDemo {
    public static void main(String[] args){
        int [][] data = {{10,2},{7,9},{11,34},{6,8}};
        for (int row = 0;row < data.length;row++){
            for (int col = 0; col < data[row].length;col++){
                System.out.println("Row: "+ row+" Col: "+ col +" Value : "+ data[row][col]);
            }
        }
        System.out.println(data[3][1]);
    }
}
