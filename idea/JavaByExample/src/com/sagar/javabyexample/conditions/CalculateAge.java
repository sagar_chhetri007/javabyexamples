package com.sagar.javabyexample.conditions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.Period;

public class CalculateAge {
    public static  void main(String[] args){
        System.out.println("Check Leap year");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String input = "";
        try{
            System.out.println("Enter Year less than 1900 in 4 digit: ");
            input = br.readLine() ;
            int year = Integer.parseInt(input);
            System.out.println("Enter month: ");
            input = br.readLine() ;
            int month = Integer.parseInt(input);
            System.out.println("Enter day : ");
            input = br.readLine() ;
            int day = Integer.parseInt(input);
            System.out.println("Given Birthdate in mm - dd - yy ;  " +month+"-"+day+"-"+ year);
            LocalDate today = LocalDate.now() ;
            LocalDate birthdate = LocalDate.of(year,month,day);
            Period period = Period.between(birthdate,today);
            System.out.println("Age : "+period.getYears()+" year "+period.getMonths()+" months "+period.getDays()+" days ");

        } catch (Exception ex ){
            System.out.println(ex.getMessage());

        }
    }

}
