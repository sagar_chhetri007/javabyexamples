package com.sagar.javabyexample.conditions;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MonthFinder {
    public static  void main(String[] args){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try{

            System.out.println("Enter Month in numeric (1 - 12): ");
            String input = br.readLine() ;

            int month = Integer.parseInt(input);
            System.out.println("Given month: " + month);
            String monthString;
            switch (month) {
                case 1:  monthString = "January";
                    break;
                case 2:  monthString = "February";
                    break;
                case 3:  monthString = "March";
                    break;
                case 4:  monthString = "April";
                    break;
                case 5:  monthString = "May";
                    break;
                case 6:  monthString = "June";
                    break;
                case 7:  monthString = "July";
                    break;
                case 8:  monthString = "August";
                    break;
                case 9:  monthString = "September";
                    break;
                case 10: monthString = "October";
                    break;
                case 11: monthString = "November";
                    break;
                case 12: monthString = "December";
                    break;
                default: monthString = "Invalid month";
                    break;
            }
            System.out.println(monthString);
        } catch (Exception ex ){
            System.out.println(ex.getMessage());

        }
    }
}
