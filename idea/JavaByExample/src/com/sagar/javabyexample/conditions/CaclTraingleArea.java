package com.sagar.javabyexample.conditions;


import java.io.BufferedReader ;
import java.io.InputStreamReader ;

public class CaclTraingleArea {
    public static  void main(String[] args){
        System.out.println("This program accepts three inputs from user and show the area of traingle");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try{
           System.out.println("Enter side A :");
           String sideA = br.readLine() ;
           System.out.println("Enter side B :");
           String sideB = br.readLine() ;
           System.out.println("Enter side C :");
           String sideC = br.readLine() ;
           float side1 = Float.parseFloat(sideA);
           float side2 = Float.parseFloat(sideB);
           float side3 = Float.parseFloat(sideC);

           System.out.println("Sides of triangle , side A : " + side1+" side B: "+ side2 +" side C: "+ side3);
           if ((side1 + side2) > side3 && (side1 + side3 ) > side2 && (side2 + side3) > side1){
               float perimeter = side1 + side2 + side3 ;
               float p = perimeter/2 ;
               double area = Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
               System.out.println("The area of triangle is : " +area);
           } else {
               System.out.println("For a traingle addition of two side should be greater than third one");
           }


        } catch (Exception ex ){
            System.out.println(ex.getMessage());

        }
    }


}
