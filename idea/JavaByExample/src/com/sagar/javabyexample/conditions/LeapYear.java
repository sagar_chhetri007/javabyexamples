package com.sagar.javabyexample.conditions;


import java.io.BufferedReader ;
import java.io.InputStreamReader ;

public class LeapYear {
    public static  void main(String[] args){
        System.out.println("Check Leap year");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try{
            System.out.println("Enter Year");
            String sideA = br.readLine() ;

            int year = Integer.parseInt(sideA);


            System.out.println("Given Year: " + year);
            if (((year % 4) == 0 && (year % 100) != 0 )|| (year % 400) == 0){

                System.out.println("The year "+year+ "is a Leap Year");
            } else {
                System.out.println("The year "+year+ "is not  a Leap Year");
            }


        } catch (Exception ex ){
            System.out.println(ex.getMessage());

        }
    }

}
