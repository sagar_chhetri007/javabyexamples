package com.sagar.javabyexample.String;

public class StringDemo {
    public static void main(String[] args){
        String str1 = "Sagar";//String is an object  str1 str2 reference to same object memory reference
        String str2 = "Sagar";
        String str3 = new String("Sagar");// creating new string object, allocating new memory
        String str4 = new String("Sagar");// creating new string object, allocating new memory
        if (str1 == str2){
            System.out.println("two strings are same");
            // == operator compare the object reference since str1 and 2 are from same string object it is true

        }else {
            System.out.println("two strings are NOT same");
        }
        if (str1 == str3){
            System.out.println("two strings are same");
            // == operator compare the object reference since str1 and 2 are from same string object it is true
        }else {
            System.out.println("two strings are NOT same");
        }
        if (str1.equals(str3)){
            System.out.println("two strings are same");
            //  Equals method compare the content of object no tmemory reference
        }else {
            System.out.println("two strings are NOT same");
        }
        //Note: String is immutable once it is created it can't be change . But String Buffer can be change dyanamically

        StringBuffer name = new StringBuffer("Sagar");
        name.append(" Chhteri");
        System.out.println(name);

    }
}
