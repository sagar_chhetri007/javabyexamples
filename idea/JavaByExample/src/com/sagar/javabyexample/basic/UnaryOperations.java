package com.sagar.javabyexample.basic;

public class UnaryOperations {
    public static void main(String [] args){
        int num1 = 12;
        int num2 = 0 ;
        System.out.println("Number1: " + num1 + "Number2: " + num2);
        num1 ++ ;
        System.out.println("After increase Number1 by ++: " + num1 + " Number2: " + num2);
        num1 -- ;
        System.out.println("After decrease Number1 by --: " + num1 + " Number2: " + num2);
        -- num1 ;
        System.out.println("After decrease Number1 by --: " + num1 + " Number2: " + num2);
        ++ num1;
        System.out.println("After increase Number1 by --: " + num1 + " Number2: " + num2);
        num2 = num1++; //Post increment , first copy the value then increase number
        System.out.println("Using postincrement Number1:  " + num1 + " Number2: " + num2);
        num2 = ++num1;// Pre increment , first increase the value then copy
        System.out.println("Using preincrement Number1:  " + num1 + " Number2: " + num2);

    }
}
