package com.sagar.javabyexample.basic;

public class SwapNumberWithoutTempVariable {
    public static void main(String [] args){
        int num1 = 12 ;
        int num2 = 4 ;
        System.out.println(" before swap num1: "+ num1 +" num2 : " + num2);
        num1 = num2 + num1 ;
        num2 = num1 - num2 ;
        num1 = num1 - num2 ;
        System.out.println(" After swap num1: "+ num1 +" num2 : " + num2);
    }
}
