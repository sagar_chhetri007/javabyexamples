package com.sagar.javabyexample.basic;

import java.io.BufferedReader ;
import java.io.InputStreamReader ;
import java.text.DecimalFormat ;

public class ConvertFahrenheitToCelcius {
    public  static void main(String [] args){
        System.out.println("This program will convert Fahrenheit temp into Celcius:");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        try {
            System.out.println("Please enter temp in Celcius :");
            String number1 = br.readLine();
            float celsius = Float.parseFloat(number1);
            float farenheit = celsius * 9/5 + 32 ;
            DecimalFormat decFormat = new DecimalFormat("0.00");
            System.out.println("The Fahrenheit temp is : " + decFormat.format(farenheit));


        }catch (Exception ex){
            System.out.println("Caught Exception :" + ex.getMessage());
            ex.printStackTrace();
        }



    }

}
