package com.sagar.javabyexample.basic;

public class SwapFirstNameLastName {
    public static void main( String [] args){
        String name = "Sagar Chhetri" ;
        System.out.println("Before swap: " + name);
        String fname = name.substring(0 , name.indexOf(" "));
        String lname = name.substring(name.indexOf(" "));
        name = lname +" "+ fname ;
        System.out.println("After swapped: " + name);
    }
}
