package com.sagar.javabyexample.basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;

public class CheckEvenOrOddWithoutCondition {
    public static void main(String[] args){
        System.out.println("Check whether the number is even or odd");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            System.out.println("Enter any positve number : ");
            String input = br.readLine();
            Integer num = Integer.parseInt(input);
            String [] check = {"Even","Odd"};
            System.out.println(check[num%2 ]);
        }catch (Exception ex){
            ex.getMessage();
        }
    }
}
