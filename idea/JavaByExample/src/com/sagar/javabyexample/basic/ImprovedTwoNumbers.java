package com.sagar.javabyexample.basic;

public class ImprovedTwoNumbers {
    public static void main(String[] args){
        System.out.print("Number1: " + args[0]);
        System.out.print("Number2: " + args[1]);
        int num1 = Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[1]);
        int result = num1 + num2 ;
        System.out.println("Result :" + result);
    }
}
